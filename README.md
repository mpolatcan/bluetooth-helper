# **BLUETOOTH HELPER API**
 This API abstracts Android's Bluetooth API and provides easy use for developers. 

## **To-Do List**
  * **Android Bluetooth LE (Low Energy)** support
  * Fixes connection between Android devices.At this time this API only provides connection between Android and other devices (computer etc.)

## **Features of version 1.0**   
  * Check Bluetooth is available on current device, enable and disabling Bluetooth adapter, discovering Bluetooth devices, enable current device discoverable to other Bluetooth devices, getting paired devices with current Bluetooth device, getting discovered devices with these methods: 

      
     *         **isBluetoothAvailable()**
     *         **enableBluetooth()**
     *         **disableBluetooth()**
     *         **startDiscoverDevices()**
     *         **enableDeviceDiscoverable(long duration)**
     *         **getDiscoveredDevices()**
     *         **getPairedDevices()**


  * Ready BroadcastReceivers for Bluetooth device found, Bluetooth state and Bluetooth scan mode changed. BluetoothHelper API registers Bluetooth device found receiver by default. You can easily register and unregister these BroadcastReceivers with these methods:

     *     **registerBluetoothDeviceFoundReceiver()**
     *     **unregisterBluetoothDeviceFoundReceiver()**
     *     **registerBluetoothStateChangeReceiver()**
     *     **unregisterBluetoothStateChangeReceiver()**
     *     **registerBluetoothScanModeChangeReceiver()**
     *     **unregisterBluetoothScanModeChangeReceiver()**
     *     **registerAllBluetoothReceivers()**
     *     **unregisterAllBluetoothReceivers()**
    
       Also you can set listeners for each receivers. These listeners are
   **BluetoothDeviceFoundListener**, **BluetoothStateChangedListener**, **BluetoothScanModeChangedListener**. For example if you want to do something when Bluetooth state changed you can do this with this API:

```
#!Java

   BluetoothHelper bluetoothHelper = new BluetoothHelper(context);
   
   bluetoothHelper.registerBluetoothStateChangeReceiver();
      
   bluetoothHelper.setBluetoothChangedListener(
         new BluetoothHelper.BluetoothStateChangedListener() {
       @Override
       public void bluetoothStateOff() {
           // do something here what you want 
       }

       @Override
       public void bluetoothStateTurningOff() {
           // do something here what you want 
       }

       @Override
       public void bluetoothStateOn() {
           // do something here what you want 
       }

       @Override
       public void bluetoothStateTurningOn() {
           // do something here what you want 
       }
    });
```
      

```
#!Java

      BluetoothDevice connectedDevice; // Bluetooth device which you want to connect.
         
      bluetoothHelper.setBluetoothClientThreadWork(new BluetoothClientThreadWork() {
          @Override
          public void clientThreadWork(BluetoothSocket bluetoothSocket) {
             // do something here what you want with BluetoothSocket
          }
       });

       bluetoothHelper.connectToDevice(connectedDevice);
```

    
 *  Ready Client and Server threads are available in this API. For example if you want to connect as Client to other Bluetooth device you can do with above code segment. **BluetoothClientThreadWork** and** BluetoothServerThreadWork** are available to implement what do you want when you connect as Client or Server to other Bluetooth device.