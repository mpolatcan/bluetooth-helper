package com.mpolatcan.bluetoothhelper;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by mpolatcan-gyte_cse on 21.01.2017.
 */
public class PairedDevicesAdapter extends RecyclerView.Adapter<PairedDevicesAdapter.PairedDevicesVH> {
    private Context context;
    private BluetoothHelper bluetoothHelper;

    public PairedDevicesAdapter(Context context, BluetoothHelper bluetoothHelper) {
        this.context = context;
        this.bluetoothHelper = bluetoothHelper;
    }

    public class PairedDevicesVH extends RecyclerView.ViewHolder {
        public TextView deviceName;
        public TextView deviceMACAddress;
        public View parentView;

        public PairedDevicesVH(View itemView) {
            super(itemView);
            parentView = itemView;
            deviceName = ((TextView) itemView.findViewById(R.id.bluetooth_device_name));
            deviceMACAddress = ((TextView) itemView.findViewById(R.id.bluetooth_device_mac_address));
        }
    }

    @Override
    public PairedDevicesVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.discovered_bluetooth_device,null);
        return new PairedDevicesVH(view);
    }

    @Override
    public void onBindViewHolder(PairedDevicesVH holder, int position) {
        final BluetoothDevice device = bluetoothHelper.getPairedDevices().get(position);
        final String deviceName = device.getName();
        String deviceMACAddress = device.getAddress();

        if (deviceName != null && !deviceName.equals("")) {
            holder.deviceName.setText("Device name: "  + deviceName);
        } else {
            holder.deviceName.setText("Device name: Unknown Device");
        }

        holder.deviceMACAddress.setText("MAC Address: " + deviceMACAddress);

        holder.parentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Connect");
                alertDialog.setCancelable(true);
                alertDialog.setMessage(
                        "Do you want to connect to device \"" +
                        (deviceName == null ? "Unknown Device" : deviceName) +
                        "\" ?");
                alertDialog.setPositiveButton("Connect", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        bluetoothHelper.connectToBluetoothDevice(device);
                    }
                });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                alertDialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return bluetoothHelper.getPairedDevices().size();
    }
}
