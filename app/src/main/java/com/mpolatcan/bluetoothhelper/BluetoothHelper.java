package com.mpolatcan.bluetoothhelper;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by mpolatcan-gyte_cse on 20.01.2017.
 */

public class BluetoothHelper {
    private static final String LOG_TAG = "BluetoothHelper";
    private static final int REQUEST_ENABLE_BLUETOOTH = 0;
    private Activity activity;
    private String serviceName;
    private String serviceUUID;
    private BluetoothStateChangedListener bluetoothStateChangedListener;
    private BluetoothDeviceFoundListener bluetoothDeviceFoundListener;
    private BluetoothScanModeChangedListener bluetoothScanModeChangedListener;
    private BluetoothServerThreadWork bluetoothServerThreadWork;
    private BluetoothClientThreadWork bluetoothClientThreadWork;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothStateChangeReceiver bluetoothStateChangeReceiver;
    private BluetoothDeviceFoundReceiver bluetoothDeviceFoundReceiver;
    private BluetoothScanModeChangeReceiver bluetoothScanModeChangeReceiver;
    private ArrayList<BluetoothDevice> discoveredDevices;

    public BluetoothHelper(Activity activity, String serviceName, String serviceUUID) {
        this.activity = activity;

        discoveredDevices = new ArrayList<>();
        this.serviceName = serviceName;
        this.serviceUUID = serviceUUID;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothStateChangeReceiver = new BluetoothStateChangeReceiver();
        bluetoothDeviceFoundReceiver = new BluetoothDeviceFoundReceiver();
        bluetoothScanModeChangeReceiver = new BluetoothScanModeChangeReceiver();

        registerBluetoothDeviceFoundReceiver();
    }

    public interface BluetoothStateChangedListener {
        void bluetoothStateOff();
        void bluetoothStateTurningOff();
        void bluetoothStateOn();
        void bluetoothStateTurningOn();
    }

    public interface BluetoothDeviceFoundListener {
        void bluetoothDeviceFound();
    }

    public interface BluetoothScanModeChangedListener {
        void bluetoothScanModeDiscoverableAndConnectable();
        void bluetoothScanModeConnectable();
        void bluetoothScanModeNone();
    }

    public interface BluetoothServerThreadWork {
        void serverThreadWork(BluetoothSocket bluetoothSocket);
    }

    public interface BluetoothClientThreadWork {
        void clientThreadWork(BluetoothSocket bluetoothSocket);
    }

    public void setBluetoothStateChangedListener(BluetoothStateChangedListener bluetoothStateChangedListener) {
        this.bluetoothStateChangedListener = bluetoothStateChangedListener;
    }

    public void setBluetoothDeviceFoundListener(BluetoothDeviceFoundListener bluetoothDeviceFoundListener) {
        this.bluetoothDeviceFoundListener = bluetoothDeviceFoundListener;
    }

    public void setBluetoothScanModeChangedListener(BluetoothScanModeChangedListener BluetoothScanModeChangedListener) {
        this.bluetoothScanModeChangedListener = BluetoothScanModeChangedListener;
    }

    public void removeBluetoothStateChangedListneer() {
        bluetoothStateChangedListener = null;
    }

    public void removeBluetoothDeviceFoundListener() {
        bluetoothDeviceFoundListener = null;
    }

    public void removeBluetoothScanModeChangedListener() {
        bluetoothScanModeChangedListener = null;
    }

    public boolean isBluetoothAvailable() {
        if (bluetoothAdapter == null) {
            return false;
        } else {
            return true;
        }
    }

    public void enableBluetooth() {
        if (isBluetoothAvailable() && !bluetoothAdapter.isEnabled()) {
            Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBluetoothIntent,REQUEST_ENABLE_BLUETOOTH);
        }
    }

    public void disableBluetooth() {
        if (isBluetoothAvailable() && bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.disable();
        }
    }

    public void enableDeviceDiscoverable(long duration){
        Intent enableDeviceDiscoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        enableDeviceDiscoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION,duration);
        activity.startActivity(enableDeviceDiscoverableIntent);
    }

    public ArrayList<BluetoothDevice> getPairedDevices() {
        return new ArrayList<>(bluetoothAdapter.getBondedDevices());
    }

    public ArrayList<BluetoothDevice> getDiscoveredDevices() {
        return discoveredDevices;
    }

    public void startDiscoverDevices() {
        discoveredDevices.clear();
        if (!bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.startDiscovery();
        }
    }

    public void registerBluetoothStateChangeReceiver() {
        IntentFilter bluetoothStateChangeIntentFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        activity.registerReceiver(bluetoothStateChangeReceiver,bluetoothStateChangeIntentFilter);
    }

    public void registerBluetoothDeviceFoundReceiver() {
        IntentFilter bluetoothDeviceFoundIntentFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        activity.registerReceiver(bluetoothDeviceFoundReceiver,bluetoothDeviceFoundIntentFilter);
    }

    public void registerBluetoothScanModeChangeReceiver() {
        IntentFilter bluetoothScanModeChangeIntentFilter = new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        activity.registerReceiver(bluetoothScanModeChangeReceiver,bluetoothScanModeChangeIntentFilter);
    }

    public void registerAllBluetoothReceivers() {
        registerBluetoothStateChangeReceiver();
        registerBluetoothDeviceFoundReceiver();
        registerBluetoothScanModeChangeReceiver();
    }

    public void unregisterBluetoothChangeReceiver() {
        activity.unregisterReceiver(bluetoothStateChangeReceiver);
    }

    public void unregisterBluetoothDeviceFoundReceiver() {
        activity.unregisterReceiver(bluetoothDeviceFoundReceiver);
    }

    public void unregisterBluetoothScanModeChangeReceiver() {
        activity.unregisterReceiver(bluetoothScanModeChangeReceiver);
    }

    public void unregisterAllBluetoothReceivers() {
        activity.unregisterReceiver(bluetoothStateChangeReceiver);
        activity.unregisterReceiver(bluetoothDeviceFoundReceiver);
        activity.unregisterReceiver(bluetoothScanModeChangeReceiver);
    }

    public void setBluetoothClientThreadWork(BluetoothClientThreadWork bluetoothClientThreadWork) {
        this.bluetoothClientThreadWork = bluetoothClientThreadWork;
    }

    public void setBluetoothServerThreadWork(BluetoothServerThreadWork bluetoothServerThreadWork) {
        this.bluetoothServerThreadWork = bluetoothServerThreadWork;
    }

    private class BluetoothStateChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,0)) {
                case BluetoothAdapter.STATE_OFF:
                    if (bluetoothStateChangedListener != null) {
                        bluetoothStateChangedListener.bluetoothStateOff();
                    }
                    break;

                case BluetoothAdapter.STATE_TURNING_OFF:
                    if (bluetoothStateChangedListener != null) {
                        bluetoothStateChangedListener.bluetoothStateTurningOff();
                    }
                    break;

                case BluetoothAdapter.STATE_ON:
                    if (bluetoothStateChangedListener != null) {
                        bluetoothStateChangedListener.bluetoothStateOn();
                    }
                    break;

                case BluetoothAdapter.STATE_TURNING_ON:
                    if (bluetoothStateChangedListener != null) {
                        bluetoothStateChangedListener.bluetoothStateTurningOn();
                    }
                    break;
            }
        }
    }

    private class BluetoothDeviceFoundReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case BluetoothDevice.ACTION_FOUND:
                    BluetoothDevice bluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                    if (isDifferentDevice(bluetoothDevice))
                        discoveredDevices.add(bluetoothDevice);

                    if (bluetoothDeviceFoundListener != null) {
                        bluetoothDeviceFoundListener.bluetoothDeviceFound();
                    }

                    break;
            }
        }
    }

    private class BluetoothScanModeChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE,0)) {
                case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
                    if (bluetoothScanModeChangedListener != null) {
                        bluetoothScanModeChangedListener.bluetoothScanModeDiscoverableAndConnectable();
                    }
                    break;

                case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
                    if (bluetoothScanModeChangedListener != null) {
                        bluetoothScanModeChangedListener.bluetoothScanModeConnectable();
                    }
                    break;

                case BluetoothAdapter.SCAN_MODE_NONE:
                    if (bluetoothScanModeChangedListener != null) {
                        bluetoothScanModeChangedListener.bluetoothScanModeNone();
                    }
                    break;
            }
        }
    }

    public boolean isDifferentDevice(BluetoothDevice device) {
        if (device.getAddress() != null &&  device.getName() != null) {
            if (device.getName().equals("") || device.getAddress().equals("")) {
                return false;
            }
        } else {
            return false;
        }

        for (BluetoothDevice bluetoothDevice : discoveredDevices) {
            if (bluetoothDevice.getAddress().equals(device.getAddress()) ||
                bluetoothDevice.getName().equals(device.getName())) {
                return false;
            }
        }

        return true;
    }

    public void connectToBluetoothDevice(BluetoothDevice device) {
        new BluetoothClientThread(device).start();
    }

    public void startBluetoothServer() {
        new BluetoothServerThread().start();
    }

    private class BluetoothServerThread extends Thread {
        private final BluetoothServerSocket btServerSocket;

        public BluetoothServerThread() {
            BluetoothServerSocket tempBtServerSocket = null;

            try {
                tempBtServerSocket = (BluetoothServerSocket) bluetoothAdapter.getClass().getMethod("listenUsingRfcommOn",
                        new Class[] {int.class}).invoke(bluetoothAdapter,10);
            } catch (Exception ex) {
                Log.d(LOG_TAG, "BluetoothServerThread: Method not found KAMOONNN!", ex);
            }

            btServerSocket = tempBtServerSocket;
        }

        @Override
        public void run() {
            BluetoothSocket connectedSocket = null;

            while (true) {
                try {
                    connectedSocket = btServerSocket.accept();
                    Log.d(LOG_TAG, "User arrived");
                } catch (IOException ex) {
                    Log.e(LOG_TAG, "Socket's accept() method failed", ex);
                    break;
                }

                if (connectedSocket != null) {
                    if (bluetoothServerThreadWork != null) {
                        bluetoothServerThreadWork.serverThreadWork(connectedSocket);
                    } else {
                        Log.w(LOG_TAG, "You haven't set client thread work for do your operations!");
                    }

                    try {
                        btServerSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }

        // Closes the connect socket and causes the thread to finish.
        public void cancel() {
            try {
                btServerSocket.close();
            } catch (IOException ex) {
                Log.e(LOG_TAG, "Could not close the connect socket", ex);
            }
        }
    }

    private class BluetoothClientThread extends Thread {
        private final BluetoothSocket btSocket;
        private final BluetoothDevice btDevice;

        public BluetoothClientThread(BluetoothDevice device) {
            BluetoothDevice remoteDevice = bluetoothAdapter.getRemoteDevice(device.getAddress());
            BluetoothSocket tempBtSocket = null;
            btDevice = device;

            try {
                tempBtSocket =(BluetoothSocket) remoteDevice.getClass().getMethod("createRfcommSocket",
                        new Class[] {int.class}).invoke(device,1);
            } catch (Exception ex) {
                Log.e(LOG_TAG, "Socket's create() method failed", ex);
            }

            btSocket = tempBtSocket;
        }

        @Override
        public void run() {
            if (bluetoothAdapter.isDiscovering())
                bluetoothAdapter.cancelDiscovery();

            try {
                btSocket.connect();
            } catch (IOException connectException) {
                Log.e(LOG_TAG, "You couldn't connect to device " + btDevice.getName(), connectException);

                try {
                    btSocket.close();
                } catch (IOException closeException) {
                    Log.e(LOG_TAG, "Could not close the client socket", closeException);
                }
                return;
            }

            Log.d(LOG_TAG, "You have connected to device " + btDevice.getName());

            if (bluetoothClientThreadWork != null) {
                bluetoothClientThreadWork.clientThreadWork(btSocket);
            } else {
                Log.w(LOG_TAG, "You haven't set client thread work for do your operations!");
            }

            cancel();
        }

        // Closes the client socket and causes the thread to finish
        public void cancel() {
            try {
                btSocket.close();
            } catch (IOException ex) {
                Log.e(LOG_TAG, "Could not close the client socket", ex);
            }
        }
    }
}
