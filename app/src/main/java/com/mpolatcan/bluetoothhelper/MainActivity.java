package com.mpolatcan.bluetoothhelper;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;


/**
 * Created by mpolatcan-gyte_cse on 21.01.2017.
 */

public class MainActivity extends AppCompatActivity {
    private BluetoothHelper bluetoothHelper;
    private RecyclerView discoveredDevices;
    private RecyclerView pairedDevices;
    private Button enableBluetoothButton;
    private Button disableBluetoothButton;
    private Button enableDiscoverableButton;
    private Button discoverDevicesButton;
    private static final String SERVICE_NAME = "BTAPI-TEST";
    private static final String SERVICE_UUID = "00001101-0000-1000-8000-00805F9B34FB";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bluetoothHelper = new BluetoothHelper(this,SERVICE_NAME,SERVICE_UUID);
        discoveredDevices = ((RecyclerView) findViewById(R.id.bluetooth_discovered_devices_list));
        pairedDevices = ((RecyclerView) findViewById(R.id.bluetooth_paired_devices_list));
        enableBluetoothButton = ((Button) findViewById(R.id.enable_bluetooth_button));
        disableBluetoothButton = ((Button) findViewById(R.id.disable_bluetooth_button));
        enableDiscoverableButton = ((Button) findViewById(R.id.enable_discoverable_button));
        discoverDevicesButton = ((Button) findViewById(R.id.discover_devices_button));

        discoveredDevices.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        discoveredDevices.setAdapter(new DiscoveredDevicesAdapter(this,bluetoothHelper));

        pairedDevices.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        pairedDevices.setAdapter(new PairedDevicesAdapter(this,bluetoothHelper));

        enableBluetoothButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothHelper.enableBluetooth();
            }
        });

        disableBluetoothButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothHelper.disableBluetooth();
            }
        });

        enableDiscoverableButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothHelper.enableDeviceDiscoverable(200);
            }
        });

        discoverDevicesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothHelper.startDiscoverDevices();
                discoveredDevices.getAdapter().notifyDataSetChanged();
            }
        });

        bluetoothHelper.setBluetoothDeviceFoundListener(new BluetoothHelper.BluetoothDeviceFoundListener() {
            @Override
            public void bluetoothDeviceFound() {
                discoveredDevices.getAdapter().notifyDataSetChanged();
            }
        });

//        bluetoothHelper.setBluetoothClientThreadWork(new BluetoothHelper.BluetoothClientThreadWork() {
//            @Override
//            public void clientThreadWork(BluetoothSocket bluetoothSocket) {
//                int i = 0;
//                try {
//                    while (true) {
//                        bluetoothSocket.getOutputStream().write(("" + i).getBytes());
//                        bluetoothSocket.getOutputStream().flush();
//                        ++i;
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        bluetoothHelper.unregisterBluetoothDeviceFoundReceiver();
        super.onDestroy();
    }
}
